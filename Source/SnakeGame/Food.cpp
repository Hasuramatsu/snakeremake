// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake))
		{
			Snake->AddSnakeElement(1,true);
			Snake->IncreaseMoveSpeed();
			SpawnFood(XSpawnRange, YSpawnRange, Snake->ElementSize);
			this->Destroy();
		}		
	}
}

void AFood::SpawnFood(float _XSpawnRange, float _YSpawnRange, int Step)
{
	_XSpawnRange = FMath::FRandRange(-_XSpawnRange/Step,_XSpawnRange/Step) * Step;
	_YSpawnRange = FMath::FRandRange(-_YSpawnRange/Step,_YSpawnRange/Step) * Step;
	GetWorld()->SpawnActor<AFood>(FoodClass,FTransform(FVector(_XSpawnRange, _YSpawnRange, 0)));
}



